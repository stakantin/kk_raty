<?php
/**
 *Created by Konstantin Kolodnitsky
 * Date: 01.11.13
 * Time: 15:20
 */
namespace kk_raty;
class Raty extends \AbstractController{
    function init(){
        parent::init();

        // add add-on locations to pathfinder
        $l = $this->api->locate('addons',__NAMESPACE__,'location');
        $addon_location = $this->api->locate('addons',__NAMESPACE__);
        $this->api->pathfinder->addLocation($addon_location,array(
            'php'=>array('lib','vendor'),
            'template'=>'templates',
            'js'=>'vendor/raty/lib'
        ))->setParent($l);

        $this->showStars();
    }
    function showStars(){
        $this->add('kk_raty/View_Raty');
    }
}